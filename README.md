
Modified the Google Authenticator PAM module to only require two-factor authentication when being accessed
from the IP-Address 192.168.1.1.  In my case, this is the address of my home router, which my server behind
the router sees all external requests coming from.  This prevents the Google Authenticator module from 
prompting for two-factor authentication when I connect from within my LAN.  This was to support a SSH
server on MAC OS X Yosemite, as pam_access.so is not available to make this work like everybody else is on Linux.

See libpam/README.md


## Original Readme:

# Google Authenticator OpenSource

[![Build Status](https://travis-ci.org/google/google-authenticator.svg?branch=master)](https://travis-ci.org/google/google-authenticator)

The Google Authenticator project includes implementations of one-time passcode
generators for several mobile platforms, as well as a pluggable authentication
module (PAM). One-time passcodes are generated using open standards developed by
the [Initiative for Open Authentication (OATH)](http://www.openauthentication.org/)
(which is unrelated to [OAuth](http://oauth.net/)). 

The Android app is in [a separate project](https://github.com/google/google-authenticator-android).

These apps are not on the app stores, and their code has diverged from what's in
the app stores, so patches here won't necessarily show up in those versions.

These implementations support the HMAC-Based One-time Password (HOTP) algorithm
specified in [RFC 4226](https://tools.ietf.org/html/rfc4226) and the Time-based
One-time Password (TOTP) algorithm specified in [RFC 6238](https://tools.ietf.org/html/rfc6238).

Further documentation is available in the [Wiki](https://github.com/google/google-authenticator/wiki).
